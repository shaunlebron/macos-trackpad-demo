# macOS Trackpad Demo

_Show raw touch points from macOS trackpad, using Storyboard and Swift._

https://user-images.githubusercontent.com/116838/236108497-ed2543de-623c-4200-8dbb-54a3ba901f61.mp4

## Building

Open `TrackpadDemo.xcodeproj/` in Xcode and from the menu click Project > Run.

(I’m using Xcode 14.2)

## Notes

I’m very new to macOS development, so pull requests and issues are welcome. I
had a lot of trouble figuring out how to put this together, but I primarily
used GitHub’s code search and stackoverflow to find usage examples of Apple’s
API functions that were mentioned here:

https://developer.apple.com/library/archive/documentation/Cocoa/Conceptual/EventOverview/HandlingTouchEvents/HandlingTouchEvents.html
